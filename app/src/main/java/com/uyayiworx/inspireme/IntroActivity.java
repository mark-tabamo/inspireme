package com.uyayiworx.inspireme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.uyayiworx.inspireme.common.BaseActivity;
import com.uyayiworx.inspireme.common.Constants;
import com.uyayiworx.inspireme.database.InspireMeDB;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;

public class IntroActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        //Init SQLite DB
        new InitThread().start();
    }

    private void initDB() {
        if(hasNewDatabase()) {
            renameLegacyDB();
            if(copyDBFromAsset()) {
                SharedPreferences pref = getSharedPreferences(null, MODE_PRIVATE);
                pref.edit().putInt(Constants.DB_VER_KEY, InspireMeDB.DATABASE_VERSION).commit();

            }else {
                rollBackLegacyDB();
            }
            deleteCopyedLegacyDB();
        }

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        Intent intent = new Intent(IntroActivity.this, QuotesActivity.class);
                        startActivity(intent);
                    }
                });
            }
        }, 3500);
    }


    private boolean hasNewDatabase() {
        boolean ret = false;
        SharedPreferences pref = getSharedPreferences(null, MODE_PRIVATE);
        int curVer = pref.getInt(Constants.DB_VER_KEY, 0);
        if(curVer < InspireMeDB.DATABASE_VERSION) {
            return true;
        }
        return false;
    }

    boolean copyDBFromAsset() {
        boolean bInitSuccess = false;
        String dbPath = new StringBuffer(getApplicationInfo().dataDir).append("/")
                .append(InspireMeDB.DB_FOLDER).append("/")
                .append(InspireMeDB.DATABASE_NAME)
                .toString();
        InputStream is = null;
        OutputStream outStream = null;
        try {
            is = getAssets().open(InspireMeDB.ASSET_PATH);
            String folderPath = new StringBuffer(getApplicationInfo().dataDir).append("/")
                    .append(InspireMeDB.DB_FOLDER).toString();
            File folder = new File(folderPath);
            if(folder.exists() == false) {
                folder.mkdirs();
            }
            File file = new File(dbPath);
            if(file.exists() == false) {
                file.createNewFile();
            }
            outStream = new FileOutputStream(file);
            byte[] buffer = new byte[4096];
            int read;
            while((read = is.read(buffer)) != -1){
                outStream.write(buffer, 0, read);
            }
            bInitSuccess = true;
        }
        catch(IOException e) {
            bInitSuccess = false;
        }
        finally {
            try {
                if(is != null) {
                    is.close();
                }
                if(outStream != null) {
                    outStream.close();
                }
            }
            catch(IOException e) {}
        }
        Log.v(getClass().getSimpleName(), "copyFromAsset result =" + bInitSuccess);
        return bInitSuccess;
    }

    //change legacy db name from dbName to dbNamebac
    void renameLegacyDB() {
        String dbPath = new StringBuffer(getApplicationInfo().dataDir).append("/")
                .append(InspireMeDB.DB_FOLDER).append("/")
                .append(InspireMeDB.DATABASE_NAME)
                .toString();

        String copypath = new StringBuffer(getApplicationInfo().dataDir).append("/")
                .append(InspireMeDB.DB_FOLDER).append("/")
                .append(InspireMeDB.DATABASE_NAME).append("bac")
                .toString();
        File copyfile = new File(copypath);

        if(copyfile.exists()) {
            copyfile.delete();
        }

        File file = new File(dbPath);
        if(file.exists()) {
            file.renameTo(copyfile);
        }
    }

    void deleteCopyedLegacyDB() {
        String copypath = new StringBuffer(getApplicationInfo().dataDir).append("/")
                .append(InspireMeDB.DB_FOLDER).append("/")
                .append(InspireMeDB.DATABASE_NAME).append("bac")
                .toString();
        File copyfile = new File(copypath);

        if(copyfile.exists()) {
            copyfile.delete();
        }
    }

    void rollBackLegacyDB() {
        String dbPath = new StringBuffer(getApplicationInfo().dataDir).append("/")
                .append(InspireMeDB.DB_FOLDER).append("/")
                .append(InspireMeDB.DATABASE_NAME).append("bac")
                .toString();

        String copypath = new StringBuffer(getApplicationInfo().dataDir).append("/")
                .append(InspireMeDB.DB_FOLDER).append("/")
                .append(InspireMeDB.DATABASE_NAME)
                .toString();
        File copyfile = new File(copypath);

        if(copyfile.exists()) {
            copyfile.delete();
        }

        File file = new File(dbPath);
        if(file.exists()) {
            file.renameTo(copyfile);
        }
    }

    class InitThread extends Thread {
        public void run() {
            initDB();
        }
    }
}
