package com.uyayiworx.inspireme;

import java.util.ArrayList;
import java.util.List;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

public class FbTwitterHelper {
 private Context context;
 private static FbTwitterHelper instance;
 private static FbTwitterHelper instance() {
  if (instance == null) {
   instance = new FbTwitterHelper();
  }
  return instance;
 }
 public FbTwitterHelper() {
  this.instance = this;
 }
 public void setContext(Context context) {
  this.context = context;
 }
 private boolean appInstalledOrNot(String uri) {
  PackageManager pm = context.getPackageManager();
  boolean app_installed = false;
  try {
   pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
   app_installed = true;
  } catch (PackageManager.NameNotFoundException e) {
   app_installed = false;
  }
  return app_installed;
 }

 public void showMessage(String msg) {
  Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
 }
 public void shareTextOnTwitter(String msg) {
  List < Intent > targetShareIntents = new ArrayList < Intent > ();
  if (appInstalledOrNot("com.twitter.android")) // Check android app is installed or not
  {
   PackageManager packageManager = context.getPackageManager(); // Create instance of PackageManager
   Intent sendIntent = new Intent(Intent.ACTION_SEND);
   sendIntent.setType("text/plain");
   //Get List of all activities
   List < ResolveInfo > resolveInfoList = packageManager.queryIntentActivities(sendIntent, 0);
   for (int j = 0; j < resolveInfoList.size(); j++) {
    ResolveInfo resInfo = resolveInfoList.get(j);
    String packageName = resInfo.activityInfo.packageName;
    //Find twitter app from list
    if (packageName.contains("com.twitter.android")) {
     Intent intent = new Intent();
     intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name)); //Create Intent with twitter app package
     intent.setAction(Intent.ACTION_SEND);
     intent.setType("text/plain");
     intent.putExtra(Intent.EXTRA_TEXT, msg); // Set message
     intent.putExtra(Intent.EXTRA_SUBJECT, "Text Sharing"); //set subject
     intent.setPackage(packageName);
     targetShareIntents.add(intent);
    }

    if (!targetShareIntents.isEmpty()) {
     Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
     chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[] {}));
     context.startActivity(chooserIntent);
    } else {
     System.out.println("Do not Have Intent");
    }
   }
  } else
   showMessage("App Not Installed");
 }
 public void postImageOnTwitter(String imageUri, String msg) {
  System.out.println("Cick on FB");
  List < Intent > targetShareIntents = new ArrayList < Intent > ();
  if (appInstalledOrNot("com.twitter.android")) {
   System.out.println("App installed");
   PackageManager packageManager = context.getPackageManager();
   Intent sendIntent = new Intent(Intent.ACTION_SEND);
   sendIntent.setType("image/*");
   List < ResolveInfo > resolveInfoList = packageManager.queryIntentActivities(sendIntent, 0);
   for (int j = 0; j < resolveInfoList.size(); j++) {
    ResolveInfo resInfo = resolveInfoList.get(j);
    String packageName = resInfo.activityInfo.packageName;
    Log.i("Package Name", packageName);
    if (packageName.contains("com.twitter.android")) {
     Intent intent = new Intent();
     intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
     intent.setAction(Intent.ACTION_SEND);
     intent.setType("image/*"); //Set MIME Type
     intent.putExtra(Intent.EXTRA_TEXT, msg);
     intent.putExtra(Intent.EXTRA_SUBJECT, "Post Image");
     Uri screenshotUri = Uri.parse(imageUri);
     intent.putExtra(Intent.EXTRA_STREAM, screenshotUri); // Pur Image to intent

     intent.setPackage(packageName);
     targetShareIntents.add(intent);
    }

    if (!targetShareIntents.isEmpty()) {
     System.out.println("Have Intent");
     Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Choose app to share");
     chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[] {}));
     context.startActivity(chooserIntent);
    } else {
     System.out.println("Do not Have Intent");
    }
   }
  } else
   showMessage("App Not Installed");
 }
}

