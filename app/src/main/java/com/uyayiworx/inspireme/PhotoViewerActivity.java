package com.uyayiworx.inspireme;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.uyayiworx.inspireme.common.BaseActivity;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoViewerActivity extends BaseActivity {
	public static final String TAG = PhotoViewerActivity.class.getSimpleName();
	public static int REQUEST_CODE = 104;
	public static final String EXTRA_IMG_URL = "itemphotovieweractivity.extra.imgurl";

	@Bind(R.id.photo)SubsamplingScaleImageView photoVw;
	String imgUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photoviewer);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        imgUrl = intent.getStringExtra(EXTRA_IMG_URL);

        Glide.with(PhotoViewerActivity.this)
                .load(imgUrl)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        //photoVw.setImage(ImageSource.cachedBitmap(resource));
                        photoVw.setImage(ImageSource.bitmap(resource));
                        hideProgress();
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        photoVw.setImage(ImageSource.resource(R.drawable.placeholder));
                        hideProgress();
                    }

                    @Override
                    public void onLoadStarted(Drawable placeholder) {
                        photoVw.setImage(ImageSource.resource(R.drawable.placeholder));
                        showProgress(true);
                    }
                })
        ;


    }

	@OnClick(R.id.btn_close)
	void onCloseClk(){
		finish();
	}


}
