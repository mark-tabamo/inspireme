package com.uyayiworx.inspireme.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mark on 003 03/11/2016.
 */


public class Quote extends CommonContent implements Parcelable {
    public int id;
    public String quote;
    public String image;

    public Quote(){}
    public Quote(int id, String quote, String image){
        this.id = id;
        this.quote = quote;
        this.image = image;
    }
    public Quote(Parcel in){
        id = in.readInt();
        quote = in.readString();
        image = in.readString();
    }
    public static final Creator<Quote> CREATOR = new Creator<Quote>() {
        @Override
        public Quote createFromParcel(Parcel in) {
            return new Quote(in);
        }

        @Override
        public Quote[] newArray(int size) {
            return new Quote[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(quote);
        parcel.writeString(image);
    }
}

