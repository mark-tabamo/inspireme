package com.uyayiworx.inspireme.common;

import android.os.Build;

import java.util.HashMap;

/**
 * Created by hyunsik on 2015. 12. 23..
 */
public class Constants {
    public static final String PREF_RES_FOLDER = "pref.resource.folder";

    //reserved for future
    public static final String DB_VER_KEY = "database_version";

    public static final int MAX_IMAGE_SIZE = 2048;

    public static final String TITLE_EXTRA = "constants.title.extra";
    public static final String ID_EXTRA = "constants.id.etra";

    public static final int DEF_PAGE_SIZE = 10;
	public static final String DEVICE_TYPE = "android";
	public static final String DEVICE_INFO = Constants.DEVICE_TYPE + " " + Build.VERSION.SDK_INT + " (" + Build.VERSION.RELEASE + ")";

	//TEMP Product Categories (CAT_NUM & CAT_NAME)
	public static final HashMap<String, String> PRODUCT_CATEGORIES = new HashMap(){
		{
			put("033015000000","주방가전");
			put("033013000000","음향가전");
			put("033007000000","생활가전");
			put("051004000000","생활가전매장");
			put("053000000000","노트북/스마트PC");
			put("054000000000","컴퓨터/일체형PC");
			put("056000000000","애플/맥북/아이맥/워치");
			put("057000000000","모니터/프로젝터");
			put("060000000000","프린터/PC주변기기");
			put("073000000000","어린이/교육");
			put("065000000000","CCTV/블랙박스");
			put("066000000000","음향가전");
			put("074000000000","패션/명품/뷰티");
			put("067000000000","스마트폰/보조배터리");
			put("068000000000","생활가전");
			put("069000000000","주방가전");
			put("070000000000","명품가방");
			put("071000000000","캠핑/아웃도어/스포츠");
			put("072000000000","주방용품/건강식품");
			put("075000000000","가구/인테리어");
			put("084000000000","노트북 사이즈별");
		}
	};

	public static final String[] MONTH_NAMES = new String[]{
			"1월",
			"2월",
			"4월",
			"5월",
			"6월",
			"7월",
			"8월",
			"9월",
			"10월",
			"11월",
			"12월",
	};
}
