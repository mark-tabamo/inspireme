package com.uyayiworx.inspireme.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hyunsik on 2015. 10. 6..
 */
public class CommonPagerAdapter extends FragmentStatePagerAdapter {
    String[] pageTitles;
    FragmentManager fragmentManager;
    private final List<Fragment> fragments = new ArrayList<>();

    public CommonPagerAdapter(FragmentManager fm, String[] pageTitles) {
        super(fm);
        this.fragmentManager = fm;
        this.pageTitles = pageTitles;
    }

    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
    }

    public void clear() {
        fragments.clear();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles[position];
    }
}
