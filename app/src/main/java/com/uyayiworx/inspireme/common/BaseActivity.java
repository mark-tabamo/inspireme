package com.uyayiworx.inspireme.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import com.uyayiworx.inspireme.App;
import com.uyayiworx.inspireme.R;
import com.uyayiworx.inspireme.database.InspireMeDB;
import java.text.NumberFormat;

public class BaseActivity extends AppCompatActivity {
    private App app;
    protected Dialog progressDialog;
    private InspireMeDB inspiremedb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		app = (App)getApplication();
        getDB();
    }

    public App getAppContext(){
        return app;
    }
    public InspireMeDB getSQLiteDB(){return inspiremedb; }


    public void showProgress() {
        progressDialog = new Dialog(this, R.style.MyProgressDialog);
        progressDialog.setCancelable(false);
        progressDialog.setContentView(new ProgressBar(this));
        progressDialog.show();
    }

    public void showProgress(boolean cancelable) {
        progressDialog = new Dialog(this, R.style.MyProgressDialog);
        progressDialog.setCancelable(cancelable);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialogInterface) {
				dialogInterface.dismiss();
			}
		});
        progressDialog.setContentView(new ProgressBar(this));
        progressDialog.show();
    }

    public void showProgress(boolean cancelable, DialogInterface.OnCancelListener onCancelListener) {
        progressDialog = new Dialog(this, R.style.MyProgressDialog);
        progressDialog.setCancelable(cancelable);
		progressDialog.setOnCancelListener(onCancelListener);
        progressDialog.setContentView(new ProgressBar(this));
        progressDialog.show();
    }
    public void hideProgress() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showNetworkConnectionError(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog);
        alertDialog.setTitle(getString(R.string.network_error));
        alertDialog.setMessage(getString(R.string.connection_error));
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        if(this.isFinishing()) return;
        alertDialog.show();
    }
    public void showAlertDialog(String title, String message){
        if(this != null && !this.isFinishing()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog);
            alertDialog.setTitle(title);
            alertDialog.setMessage(message);
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
        }
    }
    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener dialogInterface){
        if(this != null && !this.isFinishing()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog);
            alertDialog.setTitle(title);
            alertDialog.setMessage(message);
            alertDialog.setPositiveButton("Ok", dialogInterface);
            alertDialog.show();
        }
    }
	protected void getDB() {
		SharedPreferences pref = getSharedPreferences(null, MODE_PRIVATE);
		int curVer = pref.getInt(Constants.DB_VER_KEY, 0);
		if(curVer != 0) {
			inspiremedb = InspireMeDB.get(this);
		}
	}
}
