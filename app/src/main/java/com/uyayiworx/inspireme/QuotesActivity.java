package com.uyayiworx.inspireme;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.uyayiworx.inspireme.common.BaseActivity;
import com.uyayiworx.inspireme.common.RecyclerViewPositionHelper;
import com.uyayiworx.inspireme.data.Quote;
import java.util.ArrayList;
import butterknife.Bind;
import butterknife.ButterKnife;

public class QuotesActivity extends BaseActivity {
    @Bind(R.id.rcyclrMain)RecyclerView rcyclrMain;
    ArrayList<Quote> quotes;
    QuoteAdapter quoteAdapter;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    LinearLayoutManager lim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);
        ButterKnife.bind(this);

        setupList(0);
        rcyclrMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                RecyclerViewPositionHelper recyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);
                if(dy>0) {
                    visibleItemCount = recyclerView.getChildCount();
                    totalItemCount = recyclerViewHelper.getItemCount();
                    pastVisiblesItems = lim.findLastCompletelyVisibleItemPosition();

                    if (totalItemCount == pastVisiblesItems + 1 && (visibleItemCount < totalItemCount)) {
                        int offset = totalItemCount;
                        showProgress();
                        setupList(offset);
                    }
                }
            }
        });
    }

    void setupList(int offset){
        quotes = getSQLiteDB().getQuotes(offset);

        if(quoteAdapter == null) {
            quoteAdapter = new QuoteAdapter(quotes, QuotesActivity.this);
            rcyclrMain.setAdapter(quoteAdapter);
        }
        else quoteAdapter.quotes.addAll(quotes);

        if (lim == null){
            lim = new LinearLayoutManager(this);
            lim.setOrientation(LinearLayoutManager.VERTICAL);
            rcyclrMain.setLayoutManager(lim);
        }

        rcyclrMain.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgress();
                quoteAdapter.notifyDataSetChanged();
            }
        }, 1500);


    }

    class QuoteAdapter extends RecyclerView.Adapter<QuoteAdapter.ViewHolder>{
        public ArrayList<Quote> quotes;
        private Context context;

        public QuoteAdapter(ArrayList<Quote> quotes, Context context){
            this.quotes = quotes;
            this.context = context;
        }

        @Override
        public QuotesActivity.QuoteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_quote_item, parent, false);
            return new QuotesActivity.QuoteAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final Quote quote = quotes.get(position);

            Log.i("@quote", "~"+quote.id + " img=" + quote.image);

            holder.imgQuote.postDelayed(new Runnable() {
                @Override
                public void run() {
                   QuotesActivity.this.runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           Glide.with(QuotesActivity.this)
                                   .load(quote.image)
                                   .asBitmap()
                                   .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                   .animate(android.R.anim.fade_in)
                                   .placeholder(R.drawable.placeholder)
                                   .into(new SimpleTarget<Bitmap>() {
                                       @Override
                                       public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                           holder.imgQuote.setImageBitmap(resource);
                                       }
                                   });
                       }
                   });
                }
            },500);

            //Set quote text
            holder.txtQuote.setText(quote.quote);

            //View QuoteDetail
            holder.layoutItem.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(QuotesActivity.this, QuoteActivity.class);
                    intent.putExtra(QuoteActivity.EXTRA_QUOTE, quote);
                    context.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return quotes.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            @Bind(R.id.imgQuote)ImageView imgQuote;
            @Bind(R.id.txtQuote)TextView txtQuote;
            @Bind(R.id.layoutItem)LinearLayout layoutItem;

            public ViewHolder(View itemView){
                super(itemView);
                ButterKnife.bind(this, itemView);

            }
        }
    }

}
