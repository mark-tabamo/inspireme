package com.uyayiworx.inspireme;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.uyayiworx.inspireme.common.BaseActivity;
import com.uyayiworx.inspireme.data.Quote;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuoteActivity extends BaseActivity {
    public static String EXTRA_QUOTE = "QuoteActivity.EXTRA_QUOTE";
    private Quote currentQuote;

    @Bind(R.id.imgQuote)ImageView imgQuote;
    @Bind(R.id.txtQuote)TextView txtQuote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        currentQuote = intent.getParcelableExtra(EXTRA_QUOTE);
        setDetail();
    }

    void setDetail(){
        Log.i("@quote", "id=" + currentQuote.id + " quote=" + currentQuote.quote + " image=" + currentQuote.image);

        //Load Image
        Glide.with(QuoteActivity.this)
                .load(currentQuote.image)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .animate(android.R.anim.fade_in)
                .placeholder(R.drawable.placeholder)
                .into(imgQuote);

        //Set quote text
        int indexBeforeTilde = currentQuote.quote.indexOf("~");
        int indexEndQuote = currentQuote.quote.length();

        Spannable spannable = new SpannableString(currentQuote.quote);
        spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color4)),
                0,
                indexBeforeTilde,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannable.setSpan(new ForegroundColorSpan(
                        getResources().getColor(R.color.colorPrimary)),
                indexBeforeTilde,
                indexEndQuote,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);



        txtQuote.setText(spannable);
    }

    @OnClick(R.id.btnShare)
    void onShareClick(){
        doShareLink(currentQuote.quote, currentQuote.image);
    }

    @OnClick(R.id.imgQuote)
    void onImageClick(){
        Intent intent = new Intent(this, PhotoViewerActivity.class);
        intent.putExtra(PhotoViewerActivity.EXTRA_IMG_URL, currentQuote.image);
        startActivity(intent);
    }

    private void doShareLink(String text, String link) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        Intent chooserIntent = Intent.createChooser(shareIntent, "Share via");

        // for 21+, we can use EXTRA_REPLACEMENT_EXTRAS to support the specific case of Facebook
        // (only supports a link)
        // >=21: facebook=link, other=text+link
        // <=20: all=link
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if(appInstalledOrNot("com.facebook.katana")){
                shareIntent.putExtra(Intent.EXTRA_TEXT, text + " " + link);
                Bundle facebookBundle = new Bundle();
                facebookBundle.putString(Intent.EXTRA_TEXT, link);
                Bundle replacement = new Bundle();
                replacement.putBundle("com.facebook.katana", facebookBundle);
                chooserIntent.putExtra(Intent.EXTRA_REPLACEMENT_EXTRAS, replacement);
            }

        } else {
            shareIntent.putExtra(Intent.EXTRA_TEXT, link);
        }

        chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(chooserIntent);
    }


    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}
