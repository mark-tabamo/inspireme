package com.uyayiworx.inspireme.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.uyayiworx.inspireme.data.Quote;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

public class InspireMeDB extends SQLiteOpenHelper {
    private String TAG = InspireMeDB.class.getSimpleName();

    private static InspireMeDB inspiremeDB;
    private SQLiteDatabase db;

    public static final String DATABASE_NAME = "INSPIREME_CLIENT.db";
    public static final int DATABASE_VERSION = 1;
    public static final String DB_FOLDER = "databases";
    public static final String ASSET_PATH = DB_FOLDER + "/" + DATABASE_NAME;

    public static final String TABLE_QUOTES = "quotes";
    public static final String TABLE_USERPROPERTIES = "userProperties";

    public static InspireMeDB get(Context context) {
        if (inspiremeDB == null) {
            inspiremeDB = new InspireMeDB(context);
        }
        return inspiremeDB;
    }

    public static final boolean isNull() {
        return inspiremeDB == null ? true : false;
    }

    private InspireMeDB(Context context) {
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v(TAG, "onCreate db=" + db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.v(TAG, "onUpgrade db=" + db + " old=" + oldVersion + "  new=" + newVersion);
    }

    public SQLiteDatabase getDatabase() {
        return db;
    }

    public void beginTransactionNonExclusive() {
        db.beginTransactionNonExclusive();
    }

    public void setTransactionSuccessful() {
        db.setTransactionSuccessful();
    }

    public void endTransaction() {
        db.endTransaction();
    }

    public long insert(String tableName, HashMap<String, String> colVals) {

        ContentValues cv = new ContentValues();
        Set<String> keys = colVals.keySet();
        for (String key : keys)
            cv.put(key, (String) colVals.get(key));

        return db.insertWithOnConflict(tableName, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrReplace(String tableName, HashMap<String, String> colVals) {

        ContentValues cv = new ContentValues();
        Set<String> keys = colVals.keySet();
        for (String key : keys)
            cv.put(key, (String) colVals.get(key));

        return db.insertWithOnConflict(tableName, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
    }

    /**
     * @param db
     * @param colVals
     * @param tableName
     * @return
     */
    public long insert(SQLiteDatabase db, String tableName, HashMap<String, String> colVals) {

        ContentValues cv = new ContentValues();
        Set<String> keys = colVals.keySet();
        for (String key : keys)
            cv.put(key, (String) colVals.get(key));


        return db.insertWithOnConflict(tableName, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
    }


    /**
     * @param tableName
     * @param colVals
     * @param where
     * @return
     */
    public long update(String tableName, HashMap<String, String> colVals, String where) {

        ContentValues cv = new ContentValues();
        Set<String> keys = colVals.keySet();
        for (String key : keys)
            cv.put(key, (String) colVals.get(key));

        return db.updateWithOnConflict(tableName, cv, where, null, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public long clearData(String tableName) {
        return db.delete(tableName, "", null);
    }

    /**
     * @return second unit
     */
    public long getTimeStamp() {
        return Calendar.getInstance().getTime().getTime() / 1000;
    }

    public ArrayList<Quote> getQuotes(int min){
        ArrayList<Quote> quoteArrayList = new ArrayList<>();

        Cursor result = null;
        //String strQuery = "SELECT key,value FROM userProperties";
        String strQuery = "SELECT id, quote, image FROM xquotes LIMIT " + min + ",10";

        result = db.rawQuery(strQuery, null);
        Log.i("@res", "~" + result.getCount());
        if (result != null) {
            if (result.moveToFirst()) {
                do {

                    Quote quote = new Quote(result.getInt(0), result.getString(1), result.getString(2));
                    quoteArrayList.add(quote);
                } while (result.moveToNext());
            }
        }

        return quoteArrayList;
    }

}